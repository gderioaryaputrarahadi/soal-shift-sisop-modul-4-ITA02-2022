# soal-shift-sisop-modul-4-ITA02-2022
Anggota Kelompok:

-Asima Prima Yohana Tampubolon 5027201009

-Gde Rio Aryaputra Rahadi 5027201063

-Muhammad Hanif Fatihurrizqi 5027201068

# Soal 1 

Pada modul ini, praktikan diminta untuk membuat sebuah sistem dimana sistem tersebut akan otomatis mengubah nama file dari suatu direktori dengan ketentuan huruf besar akan di encode dengan atbash chipper dan huruf kecil akan di encode dengan rot13 sedangkan untuk ekstensi tidak perlu di encode. 

Jawab: 
Untuk mengerjakan program ini, digunakan 3 fungsi template yang sudah ada di dalam modul, yaitu xmp_getattr, xmp_read dan xmp_readdir. Tetapi, fungsi yang diubah hanyalah xmp_readdir karena fungsi ini digunakan untuk melakukan listing directory. Pada fungsi ini akan menerima path dari direktori tersebut. Pada soal diminta untuk melakukan encode pada file yang terdapat di directory Animeku_ sehingga fungsi ini dimodifikasi agar file yang dimodifikasi tepat berada di dalam folder Animeku_. Mount source  file system pada program ini adalah dari document. 

![](img/1.png)

Pada soal ini juga dideklarasikan fungsi find path untuk dapat mengakses dan melakukan modifikasi pada penamaan filenya. Pada step ini dilakukan penghitungan panjang pada karakter sebelum tanda titik pada penamaan file. Hal ini dilakukan agar dapat dilakukan encoding nama file tanpa melibatkan ekstensinya. 

![](img/2.png)

![](img/3.png)

Lalu, dilakukan deklarasi fungsi Animeku_ dimana isinya adalah melakukan encode dengan menggunakan pengkondisian if. Apabila character yang ingin diencode berupa huruf kapital maka akan diencode dengan algoritma atbash chipper dan apabila berupa huruf kecil maka akan di encode dengan algoritma rot13. 

![](img/4.png)

Kendala yang dialami:
Waktu pengerjaan dilaksanakan bersamaan dengan libur lebaran sehingga tidak sempat menyelesaikannya.
